package ch2OOP.Lab.Lab5Extend;

public class Programer extends Employee {

    public Programer(String fnameInput, String lnameInput, int salaryInput) {
        super(fnameInput, lnameInput, salaryInput);

    }

    public static void main(String[] args) {
        Programer em3 = new Programer("Sira", "tossaporn", 10000);
        System.out.println(em3.toString());
        em3.createWebsite(em3);
    }

    @Override
    public int getSalary() {
        return super.getSalary();
    }

    public void createWebsite(Employee employee) {
        System.out.println(employee.fname + " has been created!");
    }
}
