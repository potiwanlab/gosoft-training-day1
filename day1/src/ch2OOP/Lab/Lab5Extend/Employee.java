package ch2OOP.Lab.Lab5Extend;

public class Employee {
    public String fname;
    public String lname;
    protected int salary;

    public Employee(String fnameInput, String lnameInput, int salaryInput) {
        this.fname = fnameInput;
        this.lname = lnameInput;
        this.salary = salaryInput;
    }

    public static void main(String[] args) {
        Employee em1 = new Employee("Potiwan", "Lab-in", 10000);
        System.out.println(em1.fname);
        System.out.println(em1.toString());
    }

    @Override
    public String toString() {
        return "First Name : " + this.fname + " Last Name : " + this.lname + " salary : " + salary;
    }

    public void hello() {
        System.out.println("Hello " + this.fname);
    }

    public int getSalary() {
        return salary;
    }
}
