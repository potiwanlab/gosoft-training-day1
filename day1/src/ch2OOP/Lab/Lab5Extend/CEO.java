package ch2OOP.Lab.Lab5Extend;

public class CEO extends Employee {
    public CEO(String fnameInput, String lnameInput, int salaryInput) {
        super(fnameInput, lnameInput, salaryInput);
    }

    public static void main(String[] args) {
        CEO em2 = new CEO("Potikorn", "Lab-in", 10000);
        System.out.println(em2.getSalary());
        em2.helloFriend("ICE");
        em2.fire(em2);
        ;
    }

    @Override
    public int getSalary() {
        return super.getSalary() * 2;
    }

    public void helloFriend(String fname) {
        System.out.println("Hi I am " + this.fname + ", Nice to meet you. " + fname);
    }

    public void fire(Employee employee) {
        System.out.println(employee.fname + " has been fires!");
    }

}
