package ch2OOP.Lab;

public class Lab1MethodDemo {
    // ทดลองแก้ไข specialBonus = 0 เป็น specialBonus = 100​
    public static int specialBonus = 100;

    public static void main(String[] args) {
        // int variableFromMethod = add(1,2);
        // System.out.println(variableFromMethod);

        // ทดลองแก้ไขค่าที่ส่งเข้า add​
        // ทดลองแก้ไข method add เป็น int answer = (a+b)*2;
        int ans = add(3, 6);
        System.out.println(ans);
        int ans2 = add(ans, 5);
        System.out.println(ans2);
        int ans3 = add(ans2, 10, 10);
        System.out.println(ans3);

        // ทดลองแก้ไขค่าที่ส่งเข้า substract
        int ans4 = subtract(ans3, 4);
        System.out.println(ans4);
        // สังเกตการเปลี่ยนแปลงของ ans5
        int ans5 = addAndSubtract(3, 4, 1);
        System.out.println(ans5);
        String ans6 = add("123", "456");

        // ทดลองเพิ่ม method multiply และแก้การทำงานเป็นการคูณ​
        int ans7 = multiply(3, 5);
        System.out.println(ans7);

        // ทดลองเพิ่ม method dividedBy และแก้การทำงานเป็นการหาร​
        double ans8 = dividedBy(8, 4);
        System.out.println(ans8);
    }

    public static double dividedBy(int a, int b) {
        int answer = a * b;

        return specialBonus + answer;
    }

    public static int multiply(int a, int b) {
        int answer = a * b;

        return specialBonus + answer;
    }

    public static int add(int a, int b) {
        int answer = (a + b) * 2;

        return specialBonus + answer;
    }

    public static int add(int a, int b, int c) {
        int answer = a + b + c;

        return specialBonus + answer;
    }

    public static String add(String a, String b) {
        String answer = a + b;
        return specialBonus + answer;
    }

    public static int subtract(int a, int b) {
        int answer = a - b;

        return specialBonus + answer;
    }

    public static int addAndSubtract(int a, int b, int c) {
        int firstAnswer = add(a, b);
        int secondAnswer = subtract(firstAnswer, c);
        return specialBonus + secondAnswer;
    }
}
