package ch2OOP.Lab;

public class Lab4Static {
    // 1. ทดสอบ Static Variable อย่างน้อย 1 ตัวแปร ​
    // 2. ทดสอบ Static Method​
    // 2.1. method แบบ ไม่ return ​
    // 2.2. method แบบ retrun ​
    public String fname;
    public String lname;
    public static int age = 24;

    public static String planet = "Earth";

    public Lab4Static(String fnameInput, String lnameInput) {
        fname = fnameInput;
        lname = lnameInput;
    }

    public static void main(String[] args) {
        Lab4Static faii = new Lab4Static("Poti", "Lab");
        System.out.println(faii.planet);
        System.out.println(faii.age);

    }

    public static int gerAge() {
        return age;
    }

    public static void payMe(float money) {
        System.out.println("paid " + money);
    }

}
