package ch2OOP.Lab;

public class Lab3Employee {

    public String firstname;
    public String lastname;
    private int salary;
    // 1 ประกาศตัวแปร ชื่อ porition เป็น String เก็บตำแน่ง
    // 2 สร้าง Method เป็น public ชื่อว่า chackposition เมื่อเรียก checkposition
    // จะทำการ print ชื่อตำแหน่งออกมา
    // 3 สร้าง employee อีก 1 คน
    // 4 สร้าง method getFullname return ค่าของ Firstname เอามาต่อกับ Lastname
    public String position;

    public static void main(String[] args) {
        /**
         * Lab1Employee em1 = new Lab1Employee("Potiwan", "Lab-in", 1000000, "CEO");
         * em1.checkPosition(); Lab1Employee em2 = new Lab1Employee("Potikorn",
         * "Lab-in", 200000, "full");
         * 
         * System.out.println(em2.getFullname());
         */
        // Lab 3 ทดลองสร้าง Array จาก Class Employee​
        Lab3Employee[] employees = new Lab3Employee[3];
        for (int i = 0; i < employees.length; i++) {
            employees[i] = new Lab3Employee("firstname" + i, "lastname" + 2, 100000, "position" + i);
            System.out.println(employees[i].firstname);
        }

        System.out.println(employees[1].firstname);
        System.out.println(employees[2].lastname);
        System.out.println(employees[0].salary);

    }

    public Lab3Employee(String firstnameInput, String lastnameInput, int salaryInput, String positionInput) {
        firstname = firstnameInput;
        lastname = lastnameInput;
        salary = salaryInput;
        position = positionInput;
        int temp = 123;

    }

    public void hello() {
        System.out.println("Hello" + firstname);
    }

    public int getSalary() {
        return salary;
    }

    public String getFullname() {
        return firstname + lastname;
    }

    public void checkPosition() {
        System.out.println("My position is " + position);
    }

}
