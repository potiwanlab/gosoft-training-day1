package ch1JavaBasic.LabInClass;

public class Lab11Method {
    public static void main(String[] args) {
        /**
         * ทดลองสร้าง method แบบ Return และไม่ Return ค่า และเรียกใช้งาน method
         * ที่สร้างขึ้น โดยให้แสดงผลลัพธ์บน Console​
         */
        String hi = "Hi Faii";
        sayHi(hi);

        int number = 225;
        int sayNum = sayNumber(number);
        System.out.println(sayNum);

    }

    public static int sayNumber(int number) {
        return number;
    }

    public static void sayHi(String hi) {
        System.out.println(hi);
    }

}
