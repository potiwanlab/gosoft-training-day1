package ch1JavaBasic.LabInClass;

public class Lab3Operators {
    public static void main(String[] args) {
        // 1
        prefixAndSuffix();
        // 1&2
        Conditional();
    }

    private static void prefixAndSuffix() {
        // 1 ประกาศ int i ให้มีค่า = 20
        // เรียก i++ 5 ครั้ง แล้ว print ค่า i ออกมา​
        int i = 20;
        i++;
        i++;
        i++;
        i++;
        i++;
        System.out.println(i);

        // เรียก --i 5 ครั้ง แล้ว print ค่า i ออกมา ​
        --i;
        --i;
        --i;
        --i;
        --i;
        System.out.println(i);
    }

    private static void Conditional() {
        // 2 ทดลองใช้ && กับข้อมูลประเภท float ​
        float value1 = 1.5f;
        float value2 = 2.5f;
        if ((value1 == 1.5f) && (value2 == 2.5f))
            System.out.println("value1 is 1 AND value2 is 2");

        // 3 ทดลองใช้ || กับข้อมูลประเภท char ​
        char letter1 = 'A';
        char letter2 = 'B';
        if ((letter1 == 'A') || (letter2 == 'C'))
            System.out.println("letter1 is A OR letter2 is A");

    }

}
