package ch1JavaBasic.LabInClass;

public class Lab4Score {
    public static void main(String[] args) {
        // ประกาศตัวแปร score = อะไรก็ได้ ​
        // ถ้าscore >= 80 ให้ print ว่า Good​
        // ถ้า score >= 50 แต่น้อยกว่า 80 ให้ print ว่า normal
        // ถ้า score < 50 ให้ print ว่า fail​
        /**
         * จากข้อ 1 ให้ประกาศตัวแปร boolean ว่า isHandSome เพิ่มเข้ามาเป็นค่าอะไรก็ได้ ​
         * แก้โจทย์จาก ข้อ 2 ถ้าเกิด isHandSome = true แล้ว ไม่ว่าจะได้ score เท่าไหร่
         * ก็จะ print Good ตลอด
         */
        int score = 55;
        boolean isHandsome = true;
        if ((score >= 80) || (isHandsome == true))
            System.out.println("Good");
        else if ((score >= 50) && (score <= 80))
            System.out.println("normal");
        else if (score < 50)
            System.out.println("fail");

    }
}
