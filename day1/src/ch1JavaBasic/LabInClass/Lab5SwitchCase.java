package ch1JavaBasic.LabInClass;

public class Lab5SwitchCase {
    public static void main(String[] args) {
        // สร้างโปรแกรมตัดเกรด โดยที่​
        // เกรด A ถ้าคะแนน = 80​
        // เกรด B ถ้าคะแนน = 70 ​
        // เกรด C ถ้าคะแนน = 60​
        // เกรด D ถ้าคะแนน = 50 ​
        // เกรด F ถ้าคะแนน = 40​
        // เกรด E ถ้าคะแนนเป็นค่าอื่นๆ
        int score = 80;
        switch (score) {
        case 80:
            System.out.println("A");
            break;
        case 70:
            System.out.println("C");
            break;
        case 60:
            System.out.println("D");
            break;
        case 50:
            System.out.println("D");
            break;
        case 40:
            System.out.println("F");
            break;
        default:
            System.out.println("E");
        }

    }
}
