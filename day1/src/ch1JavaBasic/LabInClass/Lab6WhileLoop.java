package ch1JavaBasic.LabInClass;

public class Lab6WhileLoop {
    public static void main(String[] args) {
        // 1
        iWhileLoop();
        // 2
        sumNumber();
        // 3
        divideBy12();
        // 4
        arrayForEach();

    }

    public static void iWhileLoop() {
        /**
         * 1 แสดงค่า i ในแต่ละรอบ โดยการประกาศตัวแปร i มาใช้ในการนับ ในเงื่อนไขของ while
         * ถ้าหาก i น้อยกว่าหรือเท่ากับ 10 while ( i <= 10 ) แสดงค่า i ออกมา แต่ละรอบ (
         * i มีค่าเริ่มต้นเป็น 1)​
         */
        int i = 1;
        while (i <= 10) {
            System.out.println(i);
            i++;
        }
    }

    public static void sumNumber() {
        // 2 สร้างโปรแกรมหาผลรวมของตัวเลข 1 ถึง 10
        int i = 1;
        int total = 0;
        while (i <= 10) {
            System.out.println(i);
            total += i;
            i++;
        }
        System.out.println(total);
    }

    public static void divideBy12() {
        // 3 สร้างโปรแกรมหาค่าระหว่าง 1-100 ที่หาร 12 ลงตัว
        int i = 1;
        System.out.print("ค่าที่หาร 12 ลงตัว ได้แก่ ");
        while (i <= 100) {
            if (i % 12 == 0)
                System.out.print(" " + i);
            i++;
        }
        System.out.println(" ");
    }

    public static void arrayForEach() {
        /**
         * 4 ให้ประกาศ array ที่มีค่า [1,2,3,4,5] แล้วใช้ foreach ในการวน loop แล้ว
         * print ค่าออกมาทั้งหมด
         */
        int myArray[] = { 1, 2, 3, 4, 5 };
        for (int i : myArray) {
            System.out.println("Value :" + i);
        }
    }
}
