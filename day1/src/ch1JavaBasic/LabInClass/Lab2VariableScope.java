package ch1JavaBasic.LabInClass;

public class Lab2VariableScope {
    public static void main(String[] args) {
        // 1
        meow();
        // 2
        primitive();
        // 3
        finalVariable();

    }

    public static void meow() {
        /**
         * 1 ประกาศ method ชื่อว่า bark ขึ้นมา แล้วเอา method main ไปเรียก method bark
         * โดย method bark ให้มี local variable ชื่อ dogName
         * ที่กำหนดชื่อหมาเป็นอะไรก็ได้ แล้วก็ให้ print ว่า “The Dog name = xxx bark”​
         */
        String catName = "Zumo";
        System.out.println("My cat name is " + catName + " meow");

    }

    public static void primitive() {
        /**
         * 2 ให้ลองเปลี่ยนค่า primitive แล้ว print ออกมา โดยให้เปลี่ยนจาก ​ float ->
         * int​ int -> float​ double -> float char -> int ​
         */
        // float -> int
        float floatI = 5.6f;
        int floatItonewInt = (int) floatI; // Narrowing​
        System.out.println("Float value: " + floatI);
        System.out.println("float -> int = " + floatItonewInt); // 5 ​

        // int -> float​
        int intF = 5;
        float intFtoFloat = intF; // Widening​
        System.out.println("Int value: " + intF);
        System.out.println("int -> float​ =" + intFtoFloat);

        // double -> float
        double doubleF = 0.7d;
        float doubleFtoFloat = (float) doubleF;
        System.out.println("Double value: " + doubleF);
        System.out.println("double -> float = " + doubleFtoFloat);

        // char -> int
        char charI = 'A';
        int charItoInt = (int) charI;
        System.out.println("Char value: " + charI);
        System.out.println("char -> int = " + charItoInt);
    }

    private static void finalVariable() {
        /**
         * ทดลองประกาศ final variable ชื่อ hello ให้มีค่า = “Hello” แล้วบรรทัดถัดมาลองไป
         * Assign ค่าใหม่ ให้ตัวแปรนี้เป็นค่า “World” แล้วลองดู run แล้วดูว่าเกิดไรขึ้น
         * ?​
         */
        final String hello = "Hello";
        System.out.println("Hello value: " + hello);
        /*
         * hello = "World"; System.out.println("World value: " + hello);
         */
    }

}
