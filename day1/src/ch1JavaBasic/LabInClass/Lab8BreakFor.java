package ch1JavaBasic.LabInClass;

public class Lab8BreakFor {
    public static void main(String[] args) {
        // กำหนดตัวแปร count สำหรับนับจำนวนรอบ
        // count เริ่มต้นที่ 0 และไปจบที่ 20 (รวม 20 ด้วย)
        // ถ้า count มีค่าเป็น 11 คำสั่ง break ภายใน if จะทำงาน
        // ให้แสดงค่า 11 และออกจาก loop โดยไม่ทำคำสั่งด้านล่างต่อ

        for (int count = 1; count <= 20; count++) {
            System.out.println("counter : " + count);
            if (count == 11) {
                System.out.println("break : " + count);
                break;
            }
        }

    }

}
