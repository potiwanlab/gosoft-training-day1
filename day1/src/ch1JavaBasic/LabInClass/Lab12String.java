package ch1JavaBasic.LabInClass;

public class Lab12String {
    public static void main(String[] args) {
        // กำหนด String เป็นดังนี้​
        // String1 = ‘You and Me’ , String2 = ‘ you and me ‘​
        // 1 ทดลองเปรียบเทียบ String 2 String ว่าเป็นค่าเดียวกันหรือไม่​
        // 2 ใช้คำสั่งค้นหาคำใน String และแสดงคำที่ค้นหาบนหน้าจอ​
        // 3 ใช้คำสั่งหาความยาวของ String นั้น และแสดงค่าความยาว String ​
        // 4 ใช้คำสั่งตัดข้อความหรือตัด String ตำแหน่งที่ 1-4 ออก​
        // 5 ใช้คำสั่งตัดช่องว่างของประโยค​
        // 6 ใช้คำสั่งเปลี่ยน String เป็นพิมพ์ใหญ่ทั้งหมด​
        // 7 ใช้คำสั่งเปลี่ยน String2 เป็นพิมพ์ใหญ่ทั้งหมด และ ไม่มีช่องว่างซ้ายขวา
        // ด้วยการเขียน code แค่บรรทัดเดียว (ใช้ Chaining นั่นเอง)​

        // 1
        String string1 = "You and Me", string2 = " you and me ";
        if (string1.equals(string2))
            System.out.println("Equal");
        else
            System.out.println("Not Equal");

        // 2

        // 3
        System.out.println("Word 1 length : " + string1.length());
        System.out.println("Word 2 length : " + string2.length());

        // 4
        System.out.println(string1.substring(1, 4));

        // 5
        System.out.println(string2.trim());

        // 6
        System.out.println(string1.toUpperCase());

        // 7
        System.out.println(string2.toUpperCase().trim());

    }

}
