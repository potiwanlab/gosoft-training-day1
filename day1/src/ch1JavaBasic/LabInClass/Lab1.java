package ch1JavaBasic.LabInClass;

public class Lab1 {
    public static void main(String[] args) {
        // 1 ลอง Comment ทั้งแบบ Single Line และ Multiple Line
        // ลอง comment
        /**
         * ลอง Multiple Line
         */
        // 2 ประกาศตัวแปรประเภทต่างๆ
        int x = 1;
        boolean myBoolean = true;
        String text = "Hello";
        // 3 Print ทุกๆ ตัวแปร จากข้อ 2 ออกมา ในรูปแบบ “This is a String ” + variable
        System.out.println("This is a Int " + x);
        System.out.println("This is a Boolean " + myBoolean);
        System.out.println("This is a Text " + text);

    }

}
