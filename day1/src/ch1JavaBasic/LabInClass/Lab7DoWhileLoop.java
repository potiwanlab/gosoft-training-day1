package ch1JavaBasic.LabInClass;

import java.util.Scanner;

public class Lab7DoWhileLoop {
    public static void main(String[] args) {
        Scanner src = new Scanner(System.in);

        // 1 print ค่าตัวเลข 20 โดยเริ่มจากมากไปน้อย​
        int counter = 20;
        do {
            System.out.println("Counter :" + counter);
            counter--;
        } while (counter > 0);
        /**
         * 2 เขียนโปรแกรมตรวจสอบตัวเลข ว่าเป็นจำนวนคู่ หรือจำนวนคี่
         * โดยโปรแกรมจะทำงานในลูปเรื่อยๆ ถ้าหากเขายังคงกรอกเลขคู่
         * แต่ถ้ากรอกเลขคี่จะเป็นการออกจากลูป​
         */
        int num = 2;
        do {
            System.out.println("Num :" + num);
            System.out.print("Input Your num : ");
            num = src.nextInt();

        } while (num % 2 != 0);
    }

}
