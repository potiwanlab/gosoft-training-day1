package ch1JavaBasic.LabInClass;

public class Lab10Continue {
    public static void main(String[] args) {
        /**
         * ให้สร้างโปรแกรมวน loop 10 รอบ โดยมีค่าเริ่มต้นที่ 1 และไปจบ 10
         * ให้แสดงทุกค่ายกเว้นค่า 2 โดยใช้คำสั่ง continue ​ ​
         */
        for (int i = 1; i <= 10; i++) {
            if (i == 2) {
                continue;
            }
            System.out.println(i);
        }

    }

}
