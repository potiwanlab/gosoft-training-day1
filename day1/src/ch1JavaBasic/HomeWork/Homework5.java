package ch1JavaBasic.HomeWork;

public class Homework5 {
    public static void main(String[] args) {
        int num = 4;
        // draw18(num);
        // draw19(num);
        // draw20(num);
        // draw21(num); คิดไม่ออก 22 23 24 25
        draw22(num);
    }

    public static void draw22(int n) {
        int colm = n * 2 - 1;
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= i * 2; j++) {
                System.out.print("-");
            }
            System.out.println();
        }

    }

    public static void draw21(int n) {
        int row = (n * 2) - 1;
        for (int j = 1; j <= row; j++) {
            if (j <= n) {
                for (int i = n; i >= 1; i--) {
                    if (i <= j)
                        System.out.print(j + i - 1);
                    else
                        System.out.print("-");
                }
            } else {
                for (int i = n; i <= row; i++) {
                    if (i >= j)
                        System.out.print(j + i + 1);
                    else
                        System.out.print("-");
                }
            }
            System.out.println("");
        }
    }

    public static void draw20(int n) {
        //
        for (int i = n; i >= 1; i--) {
            for (int j = 1; j <= n; j++) {
                if (i <= j)
                    System.out.print("*");
                else
                    System.out.print("-");
            }
            System.out.println("");
        }

        for (int i = 2; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                if (i <= j)
                    System.out.print("*");
                else
                    System.out.print("-");
            }
            System.out.println("");
        }

    }

    public static void draw19(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                if (i <= j)
                    System.out.print("*");
                else
                    System.out.print("-");
            }
            System.out.println("");
        }
    }

    public static void draw18(int n) {
        for (int i = n; i >= 1; i--) {
            for (int j = 1; j <= n; j++) {
                if (i <= j)
                    System.out.print("*");
                else
                    System.out.print("-");
            }
            System.out.println("");
        }
    }

}
