package ch1JavaBasic.HomeWork;

import java.util.Scanner;

public class Homework4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter number");

        int enterNumber = scan.nextInt();
        for (int i = 1; i <= 12; i++) {
            System.out.println(enterNumber + " * " + i + " = " + (enterNumber * i));
        }
    }
}
