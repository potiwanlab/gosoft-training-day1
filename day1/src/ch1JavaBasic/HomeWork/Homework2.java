package ch1JavaBasic.HomeWork;

public class Homework2 {
    public static void main(String[] args) {
        // Multiplication Table​
        String[][] table = { { "1", "2", "3" }, { "4", "5", "6" }, { "7", "8", "9" } };

        multiplyTable(table);
    }

    public static void multiplyTable(String[][] table) {

        int[][] tableToInt = new int[table.length][table.length];

        for (int row = 0; row < table.length; row++) {
            for (int element = 0; element < table[row].length; element++) {
                tableToInt[row][element] = Integer.parseInt(table[row][element]);
                System.out.print(tableToInt[row][element] * 2);
            }
            System.out.println();
        }

    }
}
