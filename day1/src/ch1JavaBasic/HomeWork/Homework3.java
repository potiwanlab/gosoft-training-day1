package ch1JavaBasic.HomeWork;

public class Homework3 {

    public static void main(String[] args) {
        int num = 4;
        draw9(num);
        draw10(num);
        draw11(num);
        draw12(num);
        draw13(num);
        draw14(num);
        draw15(num);
        draw16(num);
        draw17(num);
    }

    public static void draw17(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                if (i >= j)
                    System.out.print(i);
                else
                    System.out.print("-");
            }
            System.out.println("");
        }

        for (int i = n - 1; i >= 1; i--) {
            for (int j = 1; j <= n; j++) {

                if (i >= j)
                    System.out.print(i);
                else
                    System.out.print("-");
            }
            System.out.println("");
        }
    }

    public static void draw16(int n) {
        //
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                if (i >= j)
                    System.out.print("*");
                else
                    System.out.print("-");
            }
            System.out.println("");
        }

        for (int i = n - 1; i >= 1; i--) {
            for (int j = 1; j <= n; j++) {

                if (i >= j)
                    System.out.print("*");
                else
                    System.out.print("-");
            }
            System.out.println("");
        }

    }

    public static void draw15(int n) {
        for (int i = n; i >= 1; i--) {
            for (int j = 1; j <= n; j++) {

                if (i >= j)
                    System.out.print("*");
                else
                    System.out.print("-");
            }
            System.out.println("");
        }
    }

    public static void draw14(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                if (i >= j)
                    System.out.print("*");
                else
                    System.out.print("-");
            }
            System.out.println("");
        }
    }

    public static void draw13(int n) {
        for (int i = n; i >= 1; i--) {
            for (int j = 1; j <= n; j++) {
                if (j == i)
                    System.out.print("-");
                else
                    System.out.print("*");
            }
            System.out.println("");
        }
    }

    public static void draw12(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                if (j == i)
                    System.out.print("-");
                else
                    System.out.print("*");
            }
            System.out.println("");
        }
    }

    public static void draw11(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                System.out.print(i * j);
            }
            System.out.println("");
        }
    }

    public static void draw10(int n) {
        int i = 1;
        while (i <= n) {
            System.out.println(i * 2);
            i++;
        }
    }

    public static void draw9(int n) {
        int i = 0;
        while (i < n) {
            System.out.println(i * 2);
            i++;
        }
    }

}
