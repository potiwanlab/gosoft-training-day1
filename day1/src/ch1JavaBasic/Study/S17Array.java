package ch1JavaBasic.Study;

public class S17Array {
    public static void main(String[] args) {

        initArray();
        lengthArray();

        int[] myArray = { 1, 2, 3, 4, 5 };
        printArray(myArray);
    }

    public static void printArray(int[] myArray) {

        for (int item : myArray) {
            System.out.println(item);
        }
    }

    public static void lengthArray() {
        // .length เป็นคำสั่งที่ใช้ตรวจสอบความยาวของอาเรย์​
        int[] intArray = new int[5];
        char[] charArray = { 'A', 'B', 'C', 'D' };
        String[] StringArray = { "Dog", "Cat" };
        System.out.println("intArray length: " + intArray.length);
        System.out.println("charArray length: " + charArray.length);
        System.out.println("StringArray length: " + StringArray.length);

    }

    public static void initArray() {
        // ตัวแปร[ตำแหน่ง Index] = ค่าที่ต้องการระบุ​
        int[] myIntArray = new int[5];
        myIntArray[0] = 10;
        myIntArray[1] = 30;
        myIntArray[2] = 80;
        myIntArray[3] = 5;
        myIntArray[4] = 25;
        for (int item : myIntArray) {
            System.out.println("Item :" + item);
        }
    }

}
