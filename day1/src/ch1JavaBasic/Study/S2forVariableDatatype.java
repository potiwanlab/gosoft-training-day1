package ch1JavaBasic.Study;

public class S2forVariableDatatype {
    // instance variable​
    public static int instanceInt = 50;

    public static void main(String[] args) {
        myDataType();
        myVariableExample();

    }

    public static void myVariableExample() {
        int localInt = 20; // local variable​
        System.out.println("Instance variable " + instanceInt);
        System.out.println("Local variable " + localInt);
    }

    public static void myDataType() {
        // Data type: integer​ Variable name: myInt​
        int myInt;
        myInt = 20;
        System.out.println("myInt value: " + myInt);
        int myNewInt = 50;
        System.out.println("myNewInt value: " + myNewInt);
        int number = 10;

        String greeting = "Hi";
        double myDouble = 200.4;
        boolean myBoolean = true;
        System.out.println("This is a number " + number);
        System.out.println("This is a String " + greeting);
        System.out.println("This is a Double " + myDouble);
        System.out.println("This is a Boolean " + myBoolean);
    }
}
