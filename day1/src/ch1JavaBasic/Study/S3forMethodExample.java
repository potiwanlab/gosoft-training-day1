package ch1JavaBasic.Study;

public class S3forMethodExample {

    public static void main(String[] args) {
        printFaii();
        printFaii(10, 20);
        printFaii(14, 45);

    }

    public static void printFaii(int i, int j) {
        System.out.println("sum = " + (i + j));
    }

    public static void printFaii() {
        System.out.println("I am Faii");
    }

}
