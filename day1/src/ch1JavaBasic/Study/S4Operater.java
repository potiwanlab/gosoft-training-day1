package ch1JavaBasic.Study;

public class S4Operater {

    public static void main(String[] args) {
        System.out.println("UNARY");
        int unaryVar = +1;
        System.out.println("Result " + unaryVar);// แปลงตัวแปรเป็นค่าบวก
        unaryVar--;
        System.out.println("Result " + unaryVar);// ลดจำนวนข้อมูล 1 ค่า​
        unaryVar++;
        System.out.println("Result " + unaryVar);// เพิ่มจำนวนข้อมูล 1 ค่า​
        unaryVar = -unaryVar;
        System.out.println("Result " + unaryVar);// แปลงตัวแปรเป็นค่าลบ​
        boolean success = true;
        System.out.println("Result " + success);
        System.out.println("Result " + !success);
        /**
         * เปลี่ยนข้อมูลตรรกะ !(จริง) = เท็จ​ , !(เท็จ) = จริง​
         */
        System.out.println("------------------------------------");

        System.out.println("Prefix - Suffix​");
        int i = 3;
        i++;
        System.out.println(i);
        ++i;
        System.out.println(i);
        System.out.println(++i); // เพิ่ม/ลด ค่าให้ตัวเอง 1 ค่าก่อนเริ่มทำงาน​
        System.out.println(i++); // เริ่มทำงานก่อน หลังจากนั้นเพิ่ม/ลด ค่าให้ตัวเอง 1 ค่า​
        System.out.println(i);
        System.out.println("------------------------------------");

        System.out.println("Comparison");
        int value_1 = 1;
        int value_2 = 2;
        if (value_1 == value_2)
            System.out.println("value_1 == value_2");
        if (value_1 != value_2)
            System.out.println("value_1 != value_2");
        if (value_1 > value_2)
            System.out.println("value_1 > value_2");
        if (value_1 < value_2)
            System.out.println("value_1 < value_2");
        if (value_1 <= value_2)
            System.out.println("value_1 <= value_2");

        System.out.println("Conditional");
        if ((value_1 == 1) && (value_2 == 2))
            System.out.println("value1 is 1 AND value2 is 2");
        if ((value_1 == 1) || (value_2 == 1))
            System.out.println("value1 is 1 OR value2 is 1");

    }
}
