package ch1JavaBasic.Study;

public class S11Overloading {
    public static void main(String[] args) {
        int a = 11, b = 10;
        String c = "Hello", d = "world";

        int addedInt = addition(a, b);
        String addedString = addition(c, d);
        System.out.println("addedInt: " + addedInt);
        System.out.println("addedString: " + addedString);
    }

    public static int addition(int n1, int n2) {
        return n1 + n2;
    }

    public static String addition(String n1, String n2) {
        return n1 + " " + n2;
    }

}
