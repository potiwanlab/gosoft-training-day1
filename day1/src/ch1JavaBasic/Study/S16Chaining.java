package ch1JavaBasic.Study;

public class S16Chaining {
    public static void main(String[] args) {
        oneByone();
        chaining();

    }

    public static void chaining() {
        String myName = "potiwan";
        System.out.println(setCapitalChaining(myName));
    }

    static String setCapitalChaining(String name) {
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    public static void oneByone() {
        String myName = "somchai";
        System.out.println(setCapitalOneByone(myName));
    }

    static String setCapitalOneByone(String name) {
        String firstChar = name.substring(0, 1);
        String upperCasefirstChar = firstChar.toUpperCase();
        String restOfName = name.substring(1);
        return upperCasefirstChar + restOfName;
    }

}
