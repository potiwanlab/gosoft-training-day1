package ch1JavaBasic.Study;

public class S6forSwitchStatement {
    public static void main(String[] args) {
        // Switch Statement​ Example
        int number = 20;
        switch (number) {
        case 10:
            System.out.println("10");
            break;
        case 20:
            System.out.println("20");
            break;
        case 30:
            System.out.println("30");
            break;
        default:
            System.out.println("Not in 10, 20 or 30");
        }

        // more example Switch time
        int mealTime = 3;
        String mealString = "";
        switch (mealTime) {
        case 1:
            mealString = "1: Breakfast";
            break;
        case 2:
            mealString = "2: Lunch";
            break;
        case 3:
            mealString = "3: Dinner";
            break;
        case 4:
            mealString = "4: Snack";
            break;
        default:
            System.out.println("Invalid meal time!");
        }
        System.out.println(mealString);

        // more example SwitchString
        String levelString = "Expert";
        int level = 0;
        switch (levelString) {
        case "Beginner":
            level = 1;
            break;
        case "Intermediate":
            level = 2;
            break;
        case "Expert":
            level = 3;
            break;
        default:
            level = 0;
        }
        System.out.println("Your Level is: " + level);
    }

}
