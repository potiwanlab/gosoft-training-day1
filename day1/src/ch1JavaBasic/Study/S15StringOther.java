package ch1JavaBasic.Study;

public class S15StringOther {
    public static void main(String[] args) {
        wordLength();
        wordSubstring();
        wordTrim();
        UpperCaseWord();

    }

    public static void UpperCaseWord() {
        /**
         * .toUpperCase(string)
         * เป็นเมธอดที่ใช้แปลงตัวอักษรภาษาอังกฤษเป็นตัวพิมพ์ใหญ่ทั้งหมด​
         */
        String myName = "john doe";
        System.out.println(myName.toUpperCase());
    }

    public static void wordTrim() {
        /**
         * .trim(string) เป็นเมธอดที่ใช้ตัดช่องว่างที่อยู่ทางด้านหน้า หรือด้านหลังของคำ
         * เพื่อตัดช่องว่าง และลดความยาวของคำลง​
         */
        String mySpaceWord = "   SPACE SHUTTLE   ";
        System.out.println(mySpaceWord + " length: " + mySpaceWord.length());
        String trimWord = mySpaceWord.trim();
        System.out.println(trimWord + " length: " + trimWord.length());

    }

    public static void wordSubstring() {
        /**
         * .substring(startIndex, endIndex) เป็นเมธอดที่ใช้แบ่งคำจากตัวแปรกประเภท String
         * โดยคำที่ตัดออกมาขึ้นอยู่กับช่วงของคำที่ต้องการตัดโดยที่พารามิเตอร์แรก
         * เป็นการกำหนดตำแหน่งเริ่มต้น และพารามิเตอร์ที่สอง
         * กำหนดตำแหน่งสุดท้ายในการตัดคำ​
         * 
         * ข้อควรระวัง ไม่สามารถตัดคำได้เกินความยาวของคำที่ต้องการตัด​
         */
        String word = "Java is fun!";
        System.out.println(word.substring(3));
        System.out.println(word.substring(0, 4));
        // System.out.println(word.substring(0, 20));
    }

    public static void wordLength() {
        /**
         * .length() เป็นเมธอดที่ใช้ค้นหาความยาวทั้งหมดของ String รวมถึงช่องว่าง
         * (นับว่าเป็นตัวอักษร ตามนิยามของ char) ผลลัพธ์ที่ได้เป็น int​
         */
        String word_1 = "JavaScript";
        String word_2 = "สวัสดีวันอาทิตย์";
        System.out.println("Word 1 length : " + word_1.length());
        System.out.println("Word 2 length : " + word_2.length());
    }

}
