package ch1JavaBasic.Study;

public class S9BreakCon {
    public static void main(String[] args) {
        // Break
        for (int counter = 0; counter <= 10; counter++) {
            if (counter == 5)
                break;
            System.out.println("Counter : " + counter);
        }

        // More Break
        for (int counter_1 = 0; counter_1 < 5; counter_1++) {
            for (int counter_2 = 0; counter_2 < 5; counter_2++) {
                if (counter_1 == 2 && counter_2 == 2)
                    break;
                System.out.println("Counter 1: " + counter_1 + " " + "Counter 2: " + counter_2);
            }
        }
        // continue
        for (int counter = 0; counter < 5; counter++) {
            if (counter == 2)
                continue;
            System.out.println("Counter :" + counter);
            System.out.println("Hello");
        }
        // More Continue
        for (int counter_1 = 0; counter_1 < 5; counter_1++) {
            for (int counter_2 = 0; counter_2 < 3; counter_2++) {
                if (counter_1 == 2 && counter_2 == 2)
                    continue;
                System.out.println("Counter 1: " + counter_1 + " " + "Counter 2: " + counter_2);
            }
        }
    }
}
