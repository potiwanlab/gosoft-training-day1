package ch1JavaBasic.Study;

public class S8DoWhileLoop {
    public static void main(String[] args) {
        int counter = 0;
        do {
            System.out.println("Counter :" + counter);
            counter++;
        } while (counter <= 5);
    }

}
