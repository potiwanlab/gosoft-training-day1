package ch1JavaBasic.Study;

public class S10forMethodReturn {
    public static void main(String[] args) {
        String myWord = "Hello World1";
        System.out.println(greeting(myWord));

        String greeting = "Hello World2";
        sayHello(greeting);
    }

    public static String greeting(String word) {
        return word;
    }

    public static void sayHello(String word) {
        System.out.println(word);
    }
}
