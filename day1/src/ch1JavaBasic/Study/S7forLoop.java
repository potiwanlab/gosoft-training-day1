package ch1JavaBasic.Study;

public class S7forLoop {
    public static void main(String[] args) {
        // For loop
        for (int counterFor = 0; counterFor <= 10; counterFor++) {
            System.out.println("counter of For :" + counterFor);
        }
        // For-each Loop​
        int myArray[] = { 1, 2, 3, 4, 5 };
        for (int counterForEach : myArray) {
            System.out.println("Counter of For-each :" + counterForEach);
        }

        int counterWhile = 0;
        while (counterWhile < 5) {
            System.out.println("counter of While" + counterWhile);
            counterWhile++;
        }

    }
}
