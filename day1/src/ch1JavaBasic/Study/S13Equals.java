package ch1JavaBasic.Study;

public class S13Equals {
    public static void main(String[] args) {
        myStringEquals();
        myStringCompare();

    }

    private static void myStringCompare() {
        String firstString = "HelloWorld";
        String secondString = "HelloWorld";
        /**
         * CompareTwoStrings ไม่ควรใช้ == ในการเปรียบเทียบข้อมูลประเภท String
         * เพราะเป็นการเปรียบเทียบว่าเป็นข้อมูลที่มาจากตำแหน่งอ้างอิงเดียวกันใน Memory
         * หรือไม่​
         */
        if (firstString == secondString)
            System.out.println("True");
        else
            System.out.println("False");

        if (firstString == (new String(secondString)))
            System.out.println("True");
        else
            System.out.println("False");
    }

    public static void myStringEquals() {

        /**
         * CompareStrings .equals() เป็นเมธอดที่ใช้เปรียบเทียบข้อมูลประเภท String
         * ​ผลลัพท์ที่ได้เป็น boolean​
         */
        String myString = "FirstString";
        String compareString_1 = "FirstString";
        String compareString_2 = "firstString";

        if (myString.equals(compareString_1))
            System.out.println("Equal");
        else
            System.out.println("Not equal");

        if (myString.equals(compareString_2))
            System.out.println("Equal");
        else
            System.out.println("Not equal");
    }

}
