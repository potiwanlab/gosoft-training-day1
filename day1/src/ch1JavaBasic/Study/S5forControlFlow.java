package ch1JavaBasic.Study;

public class S5forControlFlow {
    public static void main(String[] args) {
        // if Statement
        int peopleAge = 20;
        if (peopleAge > 18)
            System.out.println("You can vote.");

        // If-else Statement​
        int number = 5;
        if (number % 2 == 0) {
            System.out.println("Even number");
        } else {
            System.out.println("Odd number");
        }

        // If-else-if ladder Statement​
        int marks = 50;
        if (marks < 60) {
            System.out.println("fail");
        } else if (marks >= 60 && marks < 70) {
            System.out.println("C grade");
        } else if (marks >= 70 && marks < 80) {
            System.out.println("B grade");
        } else if (marks >= 80 && marks < 100) {
            System.out.println("A grade");
        } else {
            System.out.println("Invalid!");
        }

        // Nested if Statement​
        int age = 25;
        int weight = 48;
        if (age >= 18) {
            if (weight > 50) {
                System.out.println("OK");
            } else {
                System.out.println("Not OK");
            }
        } else {
            System.out.println("Age should greater than 18");
        }
    }

}
