package ch1JavaBasic.Study;

public class S14Employee {
    /**
     * .contains() เป็นเมธอดที่ใช้ค้นหาคำที่อยู่ในข้อมูล String ผลลัพธ์ที่ได้เป็น
     * boolean​
     */

    // จะเช็คได้ว่ามีองค์ประกอบตัวนั้นหรือป่าว
    public String firstname;
    public String lastname;
    private int age;

    public S14Employee(String firstnameInput, String lastnameInput, int ageInput) {
        firstname = firstnameInput;
        lastname = lastnameInput;
        age = ageInput;
        int temp = 123;
    }

    public void hello() {
        System.out.println("Hello " + firstname);
    }

    public int getAge() {
        return age;
    }
}
