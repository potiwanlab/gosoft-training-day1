package ch1JavaBasic.LabOptional;

public class Lab1Optional {
    public static void main(String[] args) {
        // 1 ลองหาตัวแปรประเภทอื่นๆ เองอีกสัก 2 ประเภทใน google ​
        float f = 4.1f;
        short s = 1234;

        System.out.println(f + " " + s);

        // 2 จงทำตามคำสั่งต่อไปนี้​
        /**
         * 2.1 ประกาศตัวแปรที่มีชื่อว่า miles ให้เป็นชนิดข้อมูล double
         * และให้ค่าเริ่มต้นเป็น 100 ​
         * 
         * 2.2 ประกาศตัวแปรที่เป็นค่าคงที่ ที่ชื่อว่า MILES_TO_KILOMETER
         * ให้เป็นชนิดข้อมูล double และมีค่าเริ่มต้นเป็น 1.609 ​
         * 
         * 2.3 ประกาศตัวแปรที่มีชื่อว่า kilometer ให้เป็นชนิดข้อมูล double ​
         * 
         * 2.4 ให้น าค่าในตัวแปร miles คูณกับค่าคงที่ MILES_TO_KILOMETER
         * และเก็บค่าไว้ที่ตัวแปร kilometer ​
         * 
         * 2.5 แสดงผลค่าตัวแปร kilometer ออกทางจอภาพ (console)
         */
        double miles = 100;
        double MILES_TO_KILOMETER = 1.609;
        double kilometer = miles * MILES_TO_KILOMETER;
        System.out.println(kilometer);
    }

}
