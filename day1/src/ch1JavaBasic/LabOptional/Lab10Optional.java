package ch1JavaBasic.LabOptional;

public class Lab10Optional {
    public static void main(String[] args) {
        // จงเขียนโปรแกรม เกมวัดดวง 2​
        // มีผู้เล่นทั้งหมด 10 คน ยืนประจำตำแหน่งโดยมีเลขประจำที่ (เลข 1-10)​
        // เกมนี้คัดคนเหลือเพียงครึ่งหนึ่ง​
        // โดยคนที่ได้เลข คี่ จะเป็นผู้โชคดีที่ได้อยู่ต่อทุกคน​
        // ให้แสดงค่า เลขตำแหน่งของคนที่ได้อยู่ต่อ และออกจาก loop
        // โดยไม่ทำคำสั่งด้านล่างต่อ
        int player = 10;
        for (int count = 1; count <= player; count++) {
            System.out.println("player : " + count);
            if (count % 2 != 0) {
                System.out.println("odd is lucky number : " + count);
                continue;
            }
        }
    }
}
