package ch1JavaBasic.LabOptional;

public class Lab2Optional {
    public static void main(String[] args) {
        // 1 จงประกาศชนิดของตัวแปรให้ตรงกับค่าที่ต้องการดังต่อไปนี้
        int myWeight = 74;
        float myfloatNam = 8.99f;
        char myLetter = 'A';
        boolean myBool = false;
        String myText = "Hello World";
        System.out.println("Weight: " + myWeight);
        System.out.println("Num: " + myfloatNam);
        System.out.println("Letter: " + myLetter);
        System.out.println("Boolean: " + myBool);
        System.out.println("Text: " + myText);
        /**
         * จงประกาศตัวแปรที่มีค่า 124.23 และทำการเปลี่ยนประเภทตัวแปร เพื่อให้ค่ากลายเป็น
         * 124 และลองprint ค่าที่ได้ออกมา​
         */
        float floatI = 124.23f;
        int floatItonewInt = (int) floatI;
        System.out.println("Float value: " + floatI);
        System.out.println("float -> int = " + floatItonewInt);

    }
}
