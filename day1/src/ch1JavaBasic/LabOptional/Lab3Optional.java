package ch1JavaBasic.LabOptional;

public class Lab3Optional {
    public static void main(String[] args) {
        // 1
        oddEven();
        // 2
        conditional();

    }

    private static void oddEven() {
        // 1 จงสร้าง class OddEven
        // ประกาศ int value = 1; ค่าตัวเลขที่ใช้คือ 1,2,3,4,5​
        // หากvalue มีค่าเป็นเลขคี่ ระบบจะ print คำว่า “This is Odd Number” ​
        // หากvalue มีค่าเป็นเลขคู่ ระบบจะ print คำว่า “This is Even Number”​
        int value1 = 5;
        for (int i = 1; i <= value1; i++) {
            double j = i % 2;
            if (j == 0)
                System.out.println(i + " This is Even Number");
            else
                System.out.println(i + " This is Odd Number");
        }

    }

    private static void conditional() {
        // 2 ลองใช้คำสั่ง เท่ากับ กับไม่เท่ากับ ในการเปรียบเทียบ ​
        // กำหนดค่าfirst = 2 กับ ค่าsecond =10 หากไม่เท่ากันให้แสดงผลว่า “ไม่เท่ากัน”​
        // กำหนดค่าfirst = 12 กับ ค่าsecond =12 หากเท่ากันให้แสดงผลว่า “เท่ากัน” ​
        int first = 2;
        int second = 10;

        if (first == second)
            System.out.println("เท่ากัน");
        else
            System.out.println("ไม่เท่ากัน");

        first = 12;
        second = 12;
        if (first == second)
            System.out.println("เท่ากัน");
        else
            System.out.println("ไม่เท่ากัน");

    }
}
