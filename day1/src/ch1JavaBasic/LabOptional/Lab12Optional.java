package ch1JavaBasic.LabOptional;

public class Lab12Optional {
    public static void main(String[] args) {
        /**
         * ทดลองสร้าง method และเรียกใช้งาน method ที่สร้างขึ้น ให้แสดงผลลัพธ์บน Console
         * เป็นชื่อตัวเองดังนี้​ ชื่อนามสกุล ตัวอักษรสุดท้ายเป็นตัวพิมพ์ใหญ่​ ตัวอย่าง
         * john doE​
         */
        String myName = "potiwan lab-in";
        System.out.println(setCapitalChaining(myName));
    }

    static String setCapitalChaining(String name) {
        return name.substring(0, 13) + name.substring(13).toUpperCase();
    }

}
