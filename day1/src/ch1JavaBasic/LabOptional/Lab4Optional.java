package ch1JavaBasic.LabOptional;

public class Lab4Optional {
    public static void main(String[] args) {
        // 1
        calculateWaterBill();
        // 2
        exportProduct();
        // 3
        presure();

    }

    public static void exportProduct() {
        /**
         * 2 บริษัทแห่งหนึ่งผลิตสินค้าส่งออกได้ 2000 ชิ้นต่อปี
         * ในแต่ละปีกำลังการผลิตเพิ่มขึ้นปีละ 365 ชิ้น
         * ​ห้คำนวณและแสดงผลลัพธ์ว่าใช้เวลากี่ปีบริษัทแห่งนี้จึงจะผลิตสินค้าได้มากกว่า
         * 5000 ชิ้นต่อปี
         */
        final int LIMIT_PRODUCTION = 2000;
        final int AMOUNT_PRODUCTION_PER_YEAR = 365;

        int year = 1;
        int amountOfProduct = 0;
        boolean moreThenExpected = false;
        while (!moreThenExpected) {
            amountOfProduct = LIMIT_PRODUCTION + (year * AMOUNT_PRODUCTION_PER_YEAR);
            if (amountOfProduct > 5000) {
                moreThenExpected = true;
                break;
            }
            year += 1;
        }

        System.out.println("Working on Year = " + year + " Total amount of product = " + amountOfProduct);

    }

    public static void calculateWaterBill() {
        // สร้างโปรแกรมคำนวณยอดชำระค่าน้ำประปา โดยมีเงื่อนไขดังนี้​
        // 1 - 50 หน่วย คิดหน่วยละ 4.25 บาท​
        // 51 - 100 หน่วย คิดหน่วยละ 3.25 บาท​
        // ตั้งแต่ 101 หน่วยขึ้นไป คิดหน่วยละ 2.25 บาท​

        int unit = 65;
        double total = 0;
        if (unit <= 50) {
            total = unit * 4.25;
        } else if ((unit > 50) && (unit <= 100)) {
            total = unit * 3.25;
        } else if (unit > 100) {
            total = unit * 2.25;
        }

        System.out.println("Total " + total + " Bath");
    }

    public static void presure() {
        // 3 สร้างโปรแกรมตรวจวัดความดัน โดยมีเงื่อนไขดังนี้​
        // Pressure มากกว่าหรือเท่ากับ 132 แสดงผลออกมาว่า มีความเสี่ยงสูง
        // Pressure มากกว่าหรือเท่ากับ 115 แสดงผลออกมาว่า มีความเสี่ยงปานกลาง​
        // Pressure น้อยกว่า 90 แสดงผลออกมาว่า มีความเสี่ยงต่ำ​​

        int pressure = 114;
        String risk = "";

        if (pressure >= 132) {
            risk = " มีความเสี่ยงสูง​";
        } else if (pressure >= 115) {
            risk = " มีความเสี่ยงปานกลาง​";
        } else {
            risk = " มีความเสี่ยงต่ำ​​";
        }
        System.out.println(pressure + risk);
    }
}
