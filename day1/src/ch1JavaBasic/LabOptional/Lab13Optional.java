package ch1JavaBasic.LabOptional;

public class Lab13Optional {
    public static void main(String[] args) {

        /**
         * ทดลองสร้างอาเรย์ 2 มิติ และวนลูปแล้ว print ให้ครบทุกจำนวน​ (หน้าที่แล้ว)​
         */
        int[][] twoD_Array = { { 1, 2, 3 }, { 4, 5, 6, 7 }, { 8, 9 } };
        int total = 0;
        for (int row = 0; row < twoD_Array.length; row++) {
            for (int element = 0; element < twoD_Array[row].length; element++) {
                if (element == (twoD_Array[row].length - 1)) {
                    // System.out.println(twoD_Array[row][element]);
                    total += twoD_Array[row][element];
                }
                System.out.print(twoD_Array[row][element]);
            }
            System.out.println();
        }
        /**
         * ทดลองดึงค่าในอาเรย์ 2 มิติ เอาสมาชิกตัวสุดท้ายของแต่ละแถวจากการวนลูป มาบวกกัน
         * และแสดงผลลัพธ์จากการบวกกันบน Console
         */
        System.out.println("สมาชิกตัวสุดท้ายมาบวกกัน = " + total);

    }

}
