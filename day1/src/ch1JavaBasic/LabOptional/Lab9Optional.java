package ch1JavaBasic.LabOptional;

public class Lab9Optional {
    public static void main(String[] args) {
        // กำหนดตัวแปร count สำหรับนับจำนวนรอบ
        // count เริ่มต้นที่ 0 และไปจบที่ 20 (รวม 20 ด้วย)
        // ถ้า count มีค่าเป็น 11 คำสั่ง continue ภายใน if จะทำงาน
        // ให้แสดงค่า 11 และเริ่มต้นรอบใหม่โดยไม่สนใจคำสั่งที่เหลือด้านล่าง​​
        for (int count = 1; count <= 20; count++) {
            System.out.println("counter : " + count);
            if (count == 11) {
                System.out.println("break : " + count);
                continue;
            }
        }
    }

}
