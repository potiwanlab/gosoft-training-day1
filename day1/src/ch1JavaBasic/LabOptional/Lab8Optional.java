package ch1JavaBasic.LabOptional;

public class Lab8Optional {
    public static void main(String[] args) {
        // จงเขียนโปรแกรม เกมวัดดวง
        // มีผู้เล่นทั้งหมด 10 คน ยืนประจำตำแหน่งโดยมีเลขประจำที่ (เลข 1-10)
        // เกมนี้คัดคนออกเพียงคนเดียว
        // โดยคนที่ได้เลข 7 จะถูกคัดออก
        // ให้แสดงค่า “7 is lucky number” และออกจาก loop โดยไม่ทำคำสั่งด้านล่างต่อ
        int player = 10;
        for (int count = 1; count <= player; count++) {
            System.out.println("player : " + count);
            if (count == 7) {
                System.out.println("7 is lucky number : " + count);
                break;
            }
        }
    }

}
