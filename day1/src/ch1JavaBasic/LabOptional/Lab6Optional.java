package ch1JavaBasic.LabOptional;

public class Lab6Optional {
    public static void main(String[] args) {
        // 1
        NotiTakeMedicine();
        // 2
        /**
         * จงเขียนโปรแกรมสำหรับป้ายบอกวันดิจิตัว โดยป้ายจะแสดงชื่อ ในวันนั้นๆ
         * โดยเริ่มจาก วันจันทร์ วนไปจนถึงวันอาทิตย์ เรียงลำดับ​
         */
    }

    public static void NotiTakeMedicine() {
        /**
         * 1 จงเขียนโปรแกรมแจ้งเตือนให้ทานยาลดความดัน โดนจะทำการเตือนผู้ป่วยให้ทานยาทุกๆ
         * 2 ชั่วโมง โดนยาชนิดนี้ห้ามรับประทานเกินวันละ 12 เม็ด
         * หากครบจำนวนแล้วโปรแกรมจะแจ้งเตือนว่า “ทานยาครบแล้ว อย่าทานยาเกินขนาด”​ ​
         */
        int second = 0;
        int secondsPerTwoHour = 2 * 60 * 60;
        int secondsPerhour = 60 * 60;
        int takeMedicine = 0;
        boolean canEat = true;
        while (canEat) {
            second++;
            if (second % secondsPerTwoHour == 0) {
                takeMedicine += 1;
                System.out.println("ชั่วโมงที่ " + second / secondsPerhour + " ทานยาไปแล้ว " + takeMedicine + " แล้ว");
            }

            if (takeMedicine == 12) {
                System.out.println("ทานยาครบแล้ว อย่าทานยาเกินขนาดf");
                break;
            }
        }

    }
}