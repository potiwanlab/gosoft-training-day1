
package ch1JavaBasic.LabOptional;

import java.util.Scanner;

public class Lab5Optional {
    static Scanner src = new Scanner(System.in);

    public static void main(String[] args) {
        // 1
        elevator();
        System.out.println("----------------------------------------");
        // 2
        checkSize();
    }

    public static void elevator() {
        /**
         * สร้างโปรแกรมการทำงานของลิฟต์ สำหรับ ตึก 5
         * ชั้นโดยจะให้ผู้ใช้กดปุ่มชั้นที่ต้องการไป แล้วโปรแกรมแสดงผลว่า Elevator is
         * going to ชั้นที่กด เช่นกดชั้น G จะแสดงข้อความ "Elevator is going to G floor."
         */
        System.out.print("Input Your floor : ");
        String floor = src.nextLine();

        switch (floor) {
        case "G":
            System.out.println("Elevator is going to " + floor);
            break;
        case "2":
            System.out.println("Elevator is going to " + floor);
            break;
        case "3":
            System.out.println("Elevator is going to " + floor);
            break;
        case "4":
            System.out.println("Elevator is going to " + floor);
            break;
        case "5":
            System.out.println("Elevator is going to " + floor);
            break;
        default:
            System.out.println("Invalid meal");
        }
    }

    public static void checkSize() {
        // จงเขียนโปรแกรม เช็คไซต์ของเสื่อที่ใส่ โดยใช้ switch...case statement​
        // กำหนดให้ หากระบุไซต์เป็น 29 ให้แสดงผลว่า Small​
        // กำหนดให้ หากระบุไซต์เป็น 42 ให้แสดงผลว่า Medium​
        // กำหนดให้ หากระบุไซต์เป็น 44 ให้แสดงผลว่า Large​
        // กำหนดให้ หากระบุไซต์เป็น 48 ให้แสดงผลว่า Extra Large​
        // กำหนดให้ ค่าพื้นฐาน เป็น Unknown​
        int size = 42;
        String sized = "";
        switch (size) {
        case 29:
            sized = "small";
            break;
        case 42:
            sized = "Medium​";
            break;
        case 44:
            sized = "Large​";
            break;
        case 48:
            sized = "Extra Large​";
            break;
        default:
            sized = "Unknown​​";
        }
        System.out.println(size + " = " + sized);
    }
}
