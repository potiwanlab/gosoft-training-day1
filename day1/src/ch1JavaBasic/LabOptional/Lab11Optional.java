package ch1JavaBasic.LabOptional;

public class Lab11Optional {
    public static void main(String[] args) {
        /**
         * ทดลองสร้าง method แบบ Return ค่า
         * โดยทำการคำนวณพื้นที่สี่เหลี่ยมจตุรัสเมื่อผู้ใช้กรอกขนาดเข้าไป โปรแกรมจะรับค่า
         * จากนั้นคำนวณ และแสดงค่าที่ได้ออกมา​
         */

        int side = 10;
        int area = calculateSquareArea(side);
        System.out.println("คำนวณพื้นที่สี่เหลี่ยมจัตรัสขนาด " + side + " * " + side + " = " + area);
    }

    public static int calculateSquareArea(int side) {
        return side * side;
    }

}
